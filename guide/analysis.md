# Analysis

In the analysis stage we want to get all the info on a site we can, here are some ideas of what to look for and tools to try out.

## Setting up a secure analysis browser

We'd recommend using a browser inside a VM to check out dodgy links. The sites could load unpleasant js or leave identifying cookies that you don't want on your everyday browser.

## Tips

At a minimum you want to see what the site is up to and collect WHOIS info.

Censys (www.censys.io) is a good tool for analysing hosting.

Check out the backlink checker for finding links to other sites.

Use DNS to take a look at their domain setup

## Ideas

 - automatic screenshotting of page
 - automatic collection of WHOIS


## Links:

DNS:
* [https://dnsdumpster.com/](https://dnsdumpster.com/)

Downloading certificate:
* [https://www.shellhacks.com/get-ssl-certificate-from-server-site-url-export-download/](https://www.shellhacks.com/get-ssl-certificate-from-server-site-url-export-download/)
* [https://www.golinuxcloud.com/openssl-view-certificate/](https://www.golinuxcloud.com/openssl-view-certificate/)
* `URL=www.google.com; echo $URL; echo | openssl s_client -servername $URL -connect $URL:443  | openssl x509 -noout -text`


URL reputation analysis:
* [https://scanurl.net/](https://scanurl.net/)
* [https://sitecheck.sucuri.net/](https://sitecheck.sucuri.net/)

Auto malware analysis:
* [https://any.run/](https://any.run/)
* [https://www.hybrid-analysis.com/](https://www.hybrid-analysis.com/)
* [https://www.websense.com/content/support/library/email/v85/email_help/url_sandboxing_explain_esg.aspx](https://www.websense.com/content/support/library/email/v85/email_help/url_sandboxing_explain_esg.aspx)

Automatically get pictures from sites:
* [https://github.com/maaaaz/webscreenshot](https://github.com/maaaaz/webscreenshot)


Analyse content:
* [https://github.com/kurobeats/wordhound](https://github.com/kurobeats/wordhound)
* [https://github.com/HackLikeAPornstar/StratJumbo/blob/master/chap3/wordcollector.py](https://github.com/HackLikeAPornstar/StratJumbo/blob/master/chap3/wordcollector.py)
* [https://www.autopilothq.com/blog/email-spam-trigger-words/](https://www.autopilothq.com/blog/email-spam-trigger-words/)
* [https://prospect.io/blog/455-email-spam-trigger-words-avoid-2018/](https://prospect.io/blog/455-email-spam-trigger-words-avoid-2018/)

## OSINT

Backlink checker (very cool)
* [https://ahrefs.com/backlink-checker](https://ahrefs.com/backlink-checker)

WHOIS
* [https://whois.net/](https://whois.net/)
* [https://raw.githubusercontent.com/HackLikeAPornstar/StratJumbo/master/chap1/query_whois.py](https://raw.githubusercontent.com/HackLikeAPornstar/StratJumbo/master/chap1/query_whois.py)

Infrastructure tracking
* [https://censys.io/](https://censys.io/)

Domain ownership & History:
* [https://securitytrails.com/](https://securitytrails.com/)

Blogs
* [https://www.maltego.com/blog/tracking-typosquatting-and-brand-monitoring-with-maltego/](https://www.maltego.com/blog/tracking-typosquatting-and-brand-monitoring-with-maltego/)
* [https://www.maltego.com/blog/using-maltego-to-hunt-for-phishing-subdomains/](https://www.maltego.com/blog/using-maltego-to-hunt-for-phishing-subdomains/)

Free-er version of maltego:
* [https://www.spiderfoot.net/](https://www.spiderfoot.net/)
