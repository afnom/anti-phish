# Discovery

## Custom tools

Our copy of phishing_catcher:
[https://gitlab.com/afnom/phishing_catcher](https://gitlab.com/afnom/phishing_catcher)

## Ideas

We want to develop the phishing catcher to have more useful heuristics, whitelisting and to collect additional information to inform a user.

Common issues:
 - registered certs don't imply a site: we often see landing pages, can we filter those out?
 - too much noise in the logs: whitelists of clearly legit sites

Using knowledge of whether site tries to hide from crawlers as a doginess heuristic


Maybe make some tooling to analyse whether a page looks like a faked login?


What about saving suspicious urls to check later to see if landing page has gone


## Links:

Finding sites using lists of registered domains:

* [Evilblog -- Catching phishing sites with certstream logs](https://securityevil.blogspot.com/2017/12/catching-phishing-sites-with-certstream_26.html)
* [Execute Malware Blog -- Finding Phishing Websites](https://executemalware.com/?p=258)


