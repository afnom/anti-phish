# GUIDE

At some point in the future we can reformat this to make it more readable.


The idea is to build a process and a collection of tools to catch phishing sites. In theory we already have all the tools needed, but it would be better to make the process more automatic, to make it easy for people to go through and deal with these sites

Process:
![](https://gitlab.com/afnom/anti-phish/-/blob/master/guide/images/overview.png)


## Discovery

First we need to find suspected phishing sites. Rather than wait for spam emails we can actively look for these sites out on the web. For example we can use real time logs of HTTPS certificate registrations to monitor domains people are registering certs for.


Approaches:

 - wait for suspicious emails / texts / etc. (slow)
 - Scan webservers on the internet and look for suspicious profiles (slow and hard)
 - Scan real time logs of domain registration / certificate creation and look for suspicious domain names (easy)

So we need tools that provide sites to investigate, as well as heuristics that give an indication of how suspicious the sites are. This will probably involve quite a few false starts and false positives. Once we have a site that we think is a phishing site it's time to move on to the analysis phase.

For more details / tools / ideas check out [discovery.md](https://gitlab.com/afnom/anti-phish/-/blob/master/guide/discovery.md)


## Analysis

Now we have a probable phishing site we might want to analyse it in more detail and collect evidence. We want to identify whether or not it is definitely phishing, what info the site is trying to steal, who they're impersonating etc. Also double check if it's already known by cross referencing the url agains existing blacklists.

At this point we also want technical information that's useful to a human, rather than heuristics. We might want WHOIS info, server info, what technology they're using. Are there any malicious behaviours? Sneaky tricks to hide the site?

Approaches:

 - visit the site IN A SAFE BROWSER AND VM
 - use tools to explore site behaviour
 - www.censys.io to get overview of how it's run
 - look into WHOIS data

We also want to think about how else we can use this information. It's probable that the site operators are running more than one site concurrently. Can we identify a "fingerprint" of the operators and use this to find other sites? can we find more suspicious heuristics to improve our discovery and analysis tools?

Things to look for:

 - patterns in web servers / frameworks
 - unusual / unique headers
 - unusual / unique patterns of services running on the same server
 - "leaks" of the operators techniques bleeding into the source on the fake site

For more details / tools / ideas have a look at [analysis.md](https://gitlab.com/afnom/anti-phish/-/blob/master/guide/analysis.md)


## Reporting

The final part of the process is reporting the site so it can be taken down.

Here you want to report the site to:

 - the hosting provider
 - the domain registrar
 - relevant authorities: NCSC, action fraud, google safe browsing etc.

To find domain registrar and hosting abuse contact look through the WHOIS, and also look it up on their website. You might need to fill in forms and provide screenshots. In general make sure to include the URL, IP, a screenshot and the WHOIS info in a reporting email since this info should be enough to prove to a provider the site is malicious and is hosted on their infra.