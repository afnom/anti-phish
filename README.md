# Anti-Phish

<!-- Fancy art? -->

## Overview

This repo is a central collection for running anti-phishing. It hosts a guide on techniques and ideas, along with submodules for easy access to the tools. First things first check out the guide at: [guide.md](https://gitlab.com/afnom/anti-phish/-/blob/master/guide/guide.md)

<!-- Decide on alternate format to guide: MDbook, site deployed, wiki, gitlab pages etc. -->

## TODO:

 - finish guide
 - example walkthrough
 - website analysis tool
 - infrastructure exploration tool?